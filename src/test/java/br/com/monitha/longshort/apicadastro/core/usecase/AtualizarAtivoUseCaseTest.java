package br.com.monitha.longshort.apicadastro.core.usecase;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import br.com.monitha.longshort.apicadastro.core.entity.Ativo;
import br.com.monitha.longshort.apicadastro.core.usecase.dto.AtivoRequest;
import br.com.monitha.longshort.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.monitha.longshort.apicadastro.core.usecase.gateway.BuscarPorCodigoAtivoGateway;
import br.com.monitha.longshort.apicadastro.template.AtivoTemplateLoader;
import br.com.monitha.longshort.base.dto.response.ListaErroEnum;
import br.com.monitha.longshort.base.gateway.SalvarGateway;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;

@RunWith(MockitoJUnitRunner.class)
public class AtualizarAtivoUseCaseTest {
	
	@InjectMocks
	private AtualizarAtivoUseCaseImpl usecase;
	
	@Mock
	private SalvarGateway<Ativo> gateway;
	
	@Mock
	private BuscarPorCodigoAtivoGateway buscarPorCodigoAtivoGateway;
	
	@Before
	public void before() {
		Mockito.when(gateway.salvar(Mockito.any())).thenAnswer(i -> i.getArgument(0));
	}
	
	@BeforeClass
	public static void beforeClass() {
		FixtureFactoryLoader.loadTemplates("br.com.monitha.longshort.apicadastro.template");
	}
	
	@Test
	public void deve_atualizar_ativo() {		
		Ativo ativoPetr4 = Fixture.from(Ativo.class).gimme(AtivoTemplateLoader.PETR4);
		String descricaoAntiga = ativoPetr4.getDescricao();
		
		Mockito.when(buscarPorCodigoAtivoGateway.buscarPorCodigoAtivo(ativoPetr4.getCodigo())).thenReturn(Optional.of(ativoPetr4));
		
		AtivoRequest request = new AtivoRequest();
		request.setCodigo(ativoPetr4.getCodigo());
		request.setDescricao("ATUALIZEI A DESCRICAO");
		
		AtivoResponse response = usecase.executar(request);
		
		Assert.assertNotNull(response);
		Assert.assertNotNull(response.getId());
		Assert.assertEquals(request.getCodigo(), response.getCodigo());
		Assert.assertNotEquals(descricaoAntiga, response.getDescricao());
		Assert.assertEquals("ATUALIZEI A DESCRICAO", response.getDescricao());
		
	}
	
	@Test
	public void nao_deve_atualizar_ativo_inexistente() {
		
		AtivoRequest request = new AtivoRequest();
		request.setCodigo("AA");
		request.setDescricao("DESCRICAO");
		
		Mockito.when(buscarPorCodigoAtivoGateway.buscarPorCodigoAtivo(Mockito.anyString())).thenReturn(Optional.empty());
		
		AtivoResponse response = usecase.executar(request);
		
		Assert.assertNotNull(response);
		Assert.assertNull(response.getId());
		
		
		Assert.assertEquals(request.getCodigo(), response.getCodigo());
		Assert.assertEquals(request.getDescricao(), response.getDescricao());
		
		Assert.assertNotNull(response.getResponse());
		Assert.assertEquals(1, response.getResponse().getErros().size());
		Assert.assertEquals(ListaErroEnum.REGISTRO_NAO_ENCONTRADO, response.getResponse().getErros().get(0).getTipo());
		
	}
	
	@Test
	public void nao_deve_atualizar_ativo_campos_ausentes() {
		AtivoRequest request = new AtivoRequest();
		request.setCodigo("     ");
		request.setDescricao("     ");
		AtivoResponse response = usecase.executar(request);
		
		Assert.assertNotNull(response);
		Assert.assertNull(response.getId());
		
		Assert.assertNotNull(response.getResponse());
		Assert.assertEquals(2, response.getResponse().getErros().size());
		
		Assert.assertEquals(ListaErroEnum.CAMPOS_OBRIGATORIOS, response.getResponse().getErros().get(0).getTipo());
		Assert.assertEquals(ListaErroEnum.CAMPOS_OBRIGATORIOS, response.getResponse().getErros().get(1).getTipo());
		
	}
}
