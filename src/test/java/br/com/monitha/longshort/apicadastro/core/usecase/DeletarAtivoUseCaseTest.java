package br.com.monitha.longshort.apicadastro.core.usecase;

import java.util.Optional;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import br.com.monitha.longshort.apicadastro.core.entity.Ativo;
import br.com.monitha.longshort.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.monitha.longshort.apicadastro.core.usecase.gateway.BuscarPorCodigoAtivoGateway;
import br.com.monitha.longshort.apicadastro.template.AtivoTemplateLoader;
import br.com.monitha.longshort.base.dto.response.ListaErroEnum;
import br.com.monitha.longshort.base.gateway.DeletarGateway;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;

@RunWith(MockitoJUnitRunner.class)
public class DeletarAtivoUseCaseTest {
	
	@InjectMocks
	private DeletarAtivoUseCaseImpl usecase;
	
	@Mock
	private DeletarGateway<String> deletarGateway;
	
	@Mock
	private BuscarPorCodigoAtivoGateway buscarPorCodigoAtivoGateway;
	
	@BeforeClass
	public static void beforeClass() {
		FixtureFactoryLoader.loadTemplates("br.com.monitha.longshort.apicadastro.template");
	}
	
	@Test
	public void deve_deletar_ativo() {
		Ativo ativoPetr4 = Fixture.from(Ativo.class).gimme(AtivoTemplateLoader.PETR4);
		
		Mockito.when(buscarPorCodigoAtivoGateway.buscarPorCodigoAtivo(ativoPetr4.getCodigo())).thenReturn(Optional.of(ativoPetr4));
		Mockito.when(deletarGateway.deletar(ativoPetr4.getCodigo())).thenReturn(true);
		
		AtivoResponse response = usecase.executar(ativoPetr4.getCodigo());
		
		Assert.assertEquals(response.getId(), ativoPetr4.getId());
		Assert.assertEquals(response.getCodigo(), ativoPetr4.getCodigo());
		Assert.assertEquals(response.getDescricao(), ativoPetr4.getDescricao());
		
	}
	
	@Test
	public void nao_deve_deletar_ativo_inexistente() {
		
		Ativo ativoPetr4 = Fixture.from(Ativo.class).gimme(AtivoTemplateLoader.PETR4);
		
		Mockito.when(buscarPorCodigoAtivoGateway.buscarPorCodigoAtivo(ativoPetr4.getCodigo())).thenReturn(Optional.empty());
		
		AtivoResponse response = usecase.executar(ativoPetr4.getCodigo());
		
		Assert.assertNull(response.getId());
		Assert.assertEquals(response.getCodigo(), ativoPetr4.getCodigo());
		Assert.assertNull(response.getDescricao());
		
		Assert.assertEquals(1, response.getResponse().getErros().size());
		Assert.assertEquals(ListaErroEnum.REGISTRO_NAO_ENCONTRADO,response.getResponse().getErros().get(0).getTipo());
	}
	
	@Test
	public void nao_foi_possivel_deletar() {
		
		Ativo ativoPetr4 = Fixture.from(Ativo.class).gimme(AtivoTemplateLoader.PETR4);
		
		Mockito.when(buscarPorCodigoAtivoGateway.buscarPorCodigoAtivo(ativoPetr4.getCodigo())).thenReturn(Optional.of(ativoPetr4));
		Mockito.when(deletarGateway.deletar(ativoPetr4.getCodigo())).thenReturn(false);
		
		AtivoResponse response = usecase.executar(ativoPetr4.getCodigo());
		
		Assert.assertNull(response.getId());
		Assert.assertEquals(response.getCodigo(), ativoPetr4.getCodigo());
		Assert.assertNull(response.getDescricao());
		
		Assert.assertEquals(1, response.getResponse().getErros().size());
		Assert.assertEquals(ListaErroEnum.NAO_FOI_POSSIVEL_DELETAR,response.getResponse().getErros().get(0).getTipo());
		
	}
}
