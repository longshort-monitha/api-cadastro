package br.com.monitha.longshort.apicadastro.core.usecase;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.monitha.longshort.apicadastro.core.usecase.dto.AtivoRequest;
import br.com.monitha.longshort.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.monitha.longshort.apicadastro.infra.dataprovider.repository.JpaAtivoRepository;
import br.com.monitha.longshort.apicadastro.template.AtivoRequestTemplateLoader;
import br.com.monitha.longshort.base.dto.response.ListaErroEnum;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class BuscarPorCodigoAtivoUseCaseIT {

	@Autowired
	private BuscarPorCodigoAtivoUseCase buscarUseCase;

	@Autowired
	private CadastrarAtivoUseCase cadastrarUseCase;

	@Autowired
	private JpaAtivoRepository repository;

	@Before
	public void before() {
		repository.deleteAll();
	}

	@BeforeClass
	public static void beforeClass() {
		FixtureFactoryLoader.loadTemplates("br.com.ronalan.longshort.apicadastro.template");
	}

	@Test
	public void deve_buscar_ativo_por_codigo() {
		AtivoRequest requestAtivoPetr4 = Fixture.from(AtivoRequest.class)
				.gimme(AtivoRequestTemplateLoader.PETR4);
		AtivoResponse responseCadastro = cadastrarUseCase.executar(requestAtivoPetr4);
		Assert.assertEquals(1, repository.count());

		AtivoResponse responseBuscar = buscarUseCase.executar(responseCadastro.getCodigo());
		Assert.assertNotNull(responseBuscar);
		Assert.assertEquals(responseCadastro.getCodigo(), responseBuscar.getCodigo());
		Assert.assertEquals(responseCadastro.getId(), responseBuscar.getId());
		Assert.assertEquals(responseCadastro.getDescricao(), responseBuscar.getDescricao());
		Assert.assertEquals(0, responseBuscar.getResponse().getErros().size());
	}

	@Test
	public void nao_deve_retornar_ativo_inexistente() {
		AtivoResponse responseBuscar = buscarUseCase.executar("PETR4");

		Assert.assertNotNull(responseBuscar);
		Assert.assertEquals("PETR4", responseBuscar.getCodigo());
		Assert.assertNull(responseBuscar.getId());
		Assert.assertNull(responseBuscar.getDescricao());
		Assert.assertEquals(1, responseBuscar.getResponse().getErros().size());
		Assert.assertEquals(ListaErroEnum.REGISTRO_NAO_ENCONTRADO,
		responseBuscar.getResponse().getErros().get(0).getTipo());
	}

	
}
