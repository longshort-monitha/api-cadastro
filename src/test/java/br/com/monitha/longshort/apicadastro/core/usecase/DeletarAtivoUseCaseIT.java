package br.com.monitha.longshort.apicadastro.core.usecase;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.monitha.longshort.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.monitha.longshort.apicadastro.core.usecase.dto.AtivoRequest;
import br.com.monitha.longshort.apicadastro.infra.dataprovider.repository.JpaAtivoRepository;
import br.com.monitha.longshort.apicadastro.template.AtivoRequestTemplateLoader;
import br.com.monitha.longshort.base.dto.response.ListaErroEnum;
import br.com.monitha.longshort.base.gateway.DeletarGateway;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class DeletarAtivoUseCaseIT {

	@Autowired
	private JpaAtivoRepository repository;
	
	@Autowired
	private DeletarAtivoUseCaseImpl usecase;
	
	@Autowired
	private CadastrarAtivoUseCaseImpl cadastrarUseCase;
	
	@SpyBean
	private DeletarGateway<String> deletarGateway;
	
	@Before
	public void before() {
		repository.deleteAll();
	}
	
	@BeforeClass
	public static void beforeClass() {
		FixtureFactoryLoader.loadTemplates("br.com.monitha.longshort.apicadastro.template");
	}
		
	@Test
	public void deve_deletar_ativo() {
		
		AtivoRequest cadastrarRequest = Fixture.from(AtivoRequest.class)
				.gimme(AtivoRequestTemplateLoader.PETR4);
		
		AtivoResponse response = cadastrarUseCase.executar(cadastrarRequest);
		Assert.assertEquals(1, repository.count());
		
		AtivoResponse deletarResponse = usecase.executar(cadastrarRequest.getCodigo());
		Assert.assertEquals(0, repository.count());
		
		
		Assert.assertEquals(response.getId(), deletarResponse.getId());
		Assert.assertEquals(response.getCodigo(), deletarResponse.getCodigo());
		Assert.assertEquals(response.getDescricao(), deletarResponse.getDescricao());
		
	}
	
	@Test
	public void nao_deve_deletar_ativo_inexistente() {
		
		AtivoResponse deletarResponse = usecase.executar("AAA");
		Assert.assertEquals(0, repository.count());
				
		Assert.assertNull(deletarResponse.getId());
		Assert.assertEquals(deletarResponse.getCodigo(), "AAA");
		Assert.assertNull(deletarResponse.getDescricao());
		
		Assert.assertEquals(1, deletarResponse.getResponse().getErros().size());
		Assert.assertEquals(ListaErroEnum.REGISTRO_NAO_ENCONTRADO,deletarResponse.getResponse()
				.getErros().get(0).getTipo());
	}
	
	@Test
	public void nao_foi_possivel_deletar() {
		Mockito.when(deletarGateway.deletar(Mockito.anyString())).thenReturn(false);
		
		AtivoRequest cadastrarRequest = Fixture.from(AtivoRequest.class)
				.gimme(AtivoRequestTemplateLoader.PETR4);
		
		AtivoResponse response = cadastrarUseCase.executar(cadastrarRequest);
		Assert.assertEquals(1, repository.count());
		
		AtivoResponse deletarResponse = usecase.executar(response.getCodigo());
		Assert.assertEquals(1, repository.count());
		
		Assert.assertNull(deletarResponse.getId());
		Assert.assertEquals(deletarResponse.getCodigo(), cadastrarRequest.getCodigo());
		Assert.assertNull(deletarResponse.getDescricao());
		
		Assert.assertEquals(1, deletarResponse.getResponse().getErros().size());
		Assert.assertEquals(ListaErroEnum.NAO_FOI_POSSIVEL_DELETAR,deletarResponse.getResponse()
				.getErros().get(0).getTipo());
	
	}
		
}
