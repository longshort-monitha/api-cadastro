package br.com.monitha.longshort.apicadastro.core.usecase;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.monitha.longshort.apicadastro.core.usecase.dto.AtivoRequest;
import br.com.monitha.longshort.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.monitha.longshort.apicadastro.infra.dataprovider.entity.JpaAtivoEntity;
import br.com.monitha.longshort.apicadastro.infra.dataprovider.repository.JpaAtivoRepository;
import br.com.monitha.longshort.apicadastro.template.AtivoRequestTemplateLoader;
import br.com.monitha.longshort.base.dto.response.ListaErroEnum;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class AtualizarAtivoUseCaseIT {

	@Autowired
	private JpaAtivoRepository repository;
	
	@Autowired
	private CadastrarAtivoUseCaseImpl cadastrarativousecase;
	
	@Autowired
	private AtualizarAtivoUseCaseImpl usecase;
	
	@Before
	public void before() {
		repository.deleteAll();
	}
	
	@BeforeClass
	public static void beforeClass() {
		FixtureFactoryLoader.loadTemplates("br.com.monitha.longshort.apicadastro.template");
	}
		
	@Test
	public void deve_atualizar_ativo() {
		AtivoRequest request = Fixture.from(AtivoRequest.class)
				.gimme(AtivoRequestTemplateLoader.PETR4);
		
		Assert.assertEquals(0, repository.count());
		AtivoResponse cadastrarAtivo = cadastrarativousecase.executar(request);
		Assert.assertEquals(1, repository.count());
		
		AtivoRequest requestAtualizacao = new AtivoRequest();
		requestAtualizacao.setCodigo(cadastrarAtivo.getCodigo());
		requestAtualizacao.setDescricao("ATUALIZANDO DESCRICAO PETR4");
		
		AtivoResponse responseAtualizacao = usecase.executar(requestAtualizacao);
		Assert.assertEquals(1, repository.count());
		
		Assert.assertNotNull(responseAtualizacao.getId());
		Assert.assertEquals(responseAtualizacao.getCodigo(), requestAtualizacao.getCodigo());
		Assert.assertEquals(responseAtualizacao.getCodigo(), requestAtualizacao.getCodigo());
		Assert.assertNotEquals(responseAtualizacao.getDescricao(), request.getDescricao());
		
		JpaAtivoEntity jpaAtEntity = repository.findAll().get(0);
		Assert.assertEquals(1, repository.count());
		Assert.assertEquals(responseAtualizacao.getDescricao(), jpaAtEntity.getDescricao());
	}
	
	@Test
	public void nao_deve_atualizar_ativo_inexistente() {
		AtivoRequest request = Fixture.from(AtivoRequest.class)
				.gimme(AtivoRequestTemplateLoader.PETR4);
		
		AtivoRequest requestAtualizacao = new AtivoRequest();
		requestAtualizacao.setCodigo(request.getCodigo());
		requestAtualizacao.setDescricao("ATUALIZANDO DESCRICAO PETR4");
		
		AtivoResponse responseAtualizacao = usecase.executar(requestAtualizacao);
		Assert.assertEquals(0, repository.count());
		
		Assert.assertNull(responseAtualizacao.getId());
		Assert.assertEquals(responseAtualizacao.getCodigo(), requestAtualizacao.getCodigo());
		Assert.assertEquals(responseAtualizacao.getCodigo(), requestAtualizacao.getCodigo());

		Assert.assertEquals(1, responseAtualizacao.getResponse().getErros().size());
		Assert.assertEquals(responseAtualizacao.getResponse().getErros().get(0).getTipo(), 
				ListaErroEnum.REGISTRO_NAO_ENCONTRADO);
		
	}
	
	@Test
	public void nao_deve_atualizar_ativo_campos_ausentes() {
		AtivoRequest requestAtualizacao = new AtivoRequest();
		requestAtualizacao.setCodigo("  ");
		requestAtualizacao.setDescricao("  ");
		
		AtivoResponse responseAtualizacao = usecase.executar(requestAtualizacao);
		Assert.assertEquals(0, repository.count());
		
		Assert.assertNotNull(responseAtualizacao.getResponse());
		Assert.assertNull(responseAtualizacao.getId());
		
		Assert.assertEquals(2, responseAtualizacao.getResponse().getErros().size());
		Assert.assertEquals(responseAtualizacao.getResponse().getErros().get(0).getTipo(), ListaErroEnum.CAMPOS_OBRIGATORIOS);
		Assert.assertEquals(responseAtualizacao.getResponse().getErros().get(1).getTipo(), ListaErroEnum.CAMPOS_OBRIGATORIOS);
		
	}
	
}
