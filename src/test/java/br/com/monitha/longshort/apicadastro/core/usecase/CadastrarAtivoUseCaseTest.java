package br.com.monitha.longshort.apicadastro.core.usecase;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import br.com.monitha.longshort.apicadastro.core.entity.Ativo;
import br.com.monitha.longshort.apicadastro.core.usecase.dto.AtivoRequest;
import br.com.monitha.longshort.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.monitha.longshort.apicadastro.core.usecase.gateway.BuscarPorCodigoAtivoGateway;
import br.com.monitha.longshort.apicadastro.core.usecase.gateway.GerarEventoCadastroAtivoGateway;
import br.com.monitha.longshort.apicadastro.template.AtivoRequestTemplateLoader;
import br.com.monitha.longshort.base.dto.response.ListaErroEnum;
import br.com.monitha.longshort.base.gateway.SalvarGateway;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;

@RunWith(MockitoJUnitRunner.class)
public class CadastrarAtivoUseCaseTest {
	
	@InjectMocks
	private CadastrarAtivoUseCaseImpl usecase;
	
	@Mock
	private SalvarGateway<Ativo> gateway;
	
	@Mock
	private BuscarPorCodigoAtivoGateway buscarPorCodigoAtivoGateway;
	
	@Mock
	private GerarEventoCadastroAtivoGateway gerarEventoGateway;
	
	@Before
	public void before() {
		Mockito.when(gateway.salvar(Mockito.any())).thenAnswer(i -> i.getArgument(0));
	}
	
	@BeforeClass
	public static void beforeClass() {
		FixtureFactoryLoader.loadTemplates("br.com.monitha.longshort.apicadastro.template");
	}
	
	@Test
	public void deve_salvar_ativo() {
		AtivoRequest request = Fixture.from(AtivoRequest.class)
				.gimme(AtivoRequestTemplateLoader.RANDOM);
		
		AtivoResponse response = usecase.executar(request);
		
		Assert.assertNotNull(response);
		Assert.assertNotNull(response.getId());
		Assert.assertEquals(request.getCodigo(), response.getCodigo());
		Assert.assertEquals(request.getDescricao(), response.getDescricao());
		Mockito.verify(gerarEventoGateway).gerarEvento(Mockito.any());
		
	}
	
	@Test
	public void nao_deve_salvar_duplicado() {
		AtivoRequest request = Fixture.from(AtivoRequest.class).gimme(AtivoRequestTemplateLoader.PETR4);
		Mockito.when(buscarPorCodigoAtivoGateway.buscarPorCodigoAtivo(request.getCodigo())).thenReturn(Optional.of(new Ativo()));
		
		AtivoResponse response = usecase.executar(request);
		
		Assert.assertNotNull(response);
		Assert.assertNull(response.getId());
		
		Assert.assertNotNull(response.getResponse());
		Assert.assertEquals(1, response.getResponse().getErros().size());
		Assert.assertEquals(request.getCodigo(), response.getCodigo());
		Assert.assertEquals(request.getDescricao(), response.getDescricao());
		Assert.assertEquals(ListaErroEnum.DUPLICIDADE, response.getResponse().getErros().get(0).getTipo());
		Mockito.verify(gerarEventoGateway, Mockito.times(0)).gerarEvento(Mockito.any());
		
	}
	
	@Test
	public void nao_deve_salvar_ativo_ausente() {
		AtivoRequest request = new AtivoRequest();
		request.setCodigo("          ");
		AtivoResponse response = usecase.executar(request);
		
		Assert.assertNotNull(response);
		Assert.assertNull(response.getId());
		
		Assert.assertNotNull(response.getResponse());
		Assert.assertEquals(2, response.getResponse().getErros().size());
		
		Assert.assertEquals(ListaErroEnum.CAMPOS_OBRIGATORIOS, response.getResponse().getErros().get(0).getTipo());
		Assert.assertEquals(ListaErroEnum.CAMPOS_OBRIGATORIOS, response.getResponse().getErros().get(1).getTipo());
		Mockito.verify(gerarEventoGateway, Mockito.times(0)).gerarEvento(Mockito.any());
		
	}
}
