package br.com.monitha.longshort.apicadastro.core.usecase;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.monitha.longshort.apicadastro.core.usecase.dto.AtivoRequest;
import br.com.monitha.longshort.apicadastro.core.usecase.dto.PaginaAtivoResponse;
import br.com.monitha.longshort.apicadastro.core.usecase.gateway.ConfiguracaoGateway;
import br.com.monitha.longshort.apicadastro.infra.dataprovider.repository.JpaAtivoRepository;
import br.com.monitha.longshort.apicadastro.template.AtivoRequestTemplateLoader;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class BuscarAtivosUseCaseIT {
	
	@Autowired
	private BuscarAtivosUseCase buscarAtivosUseCase;

	@Autowired
	private CadastrarAtivoUseCase cadastrarUseCase;

	@Autowired
	private JpaAtivoRepository repository;
	
	@SpyBean
	private ConfiguracaoGateway configuracaoGateway;

	@Before
	public void before() {
		repository.deleteAll();
		Mockito.when(configuracaoGateway.getQuantidadeRegistrosPorPagina()).thenReturn(2);
	}

	@BeforeClass
	public static void beforeClass() {
		FixtureFactoryLoader.loadTemplates("br.com.monitha.longshort.apicadastro.template");
	}

	@Test
	public void deve_buscar_ativos_paginado() { 
		
		for (int i = 0; i < 5; i++) {
			cadastrarUseCase.executar(Fixture.from(AtivoRequest.class).gimme(AtivoRequestTemplateLoader.RANDOM));
		}
		Assert.assertEquals(5, repository.count());
		
		PaginaAtivoResponse paginadoResponse = buscarAtivosUseCase.executar(0);
		Assert.assertEquals(2, paginadoResponse.getAtivos().size());
		Assert.assertEquals(Integer.valueOf(0), paginadoResponse.getPaginaAtual());
		Assert.assertEquals(Integer.valueOf(2), paginadoResponse.getQtdRegistrosPagina());
		Assert.assertEquals(Integer.valueOf(5), paginadoResponse.getQtdRegistrosTotais());
		
		paginadoResponse = buscarAtivosUseCase.executar(2);
		Assert.assertEquals(1, paginadoResponse.getAtivos().size());
		Assert.assertEquals(Integer.valueOf(2), paginadoResponse.getPaginaAtual());
		Assert.assertEquals(Integer.valueOf(1), paginadoResponse.getQtdRegistrosPagina());
		Assert.assertEquals(Integer.valueOf(5), paginadoResponse.getQtdRegistrosTotais());
	}
	
	@Test
	public void deve_buscar_ativos_paginado_mesmo_com_pagina_invalida() {
		
		for (int i = 0; i < 5; i++) {
			cadastrarUseCase.executar(Fixture.from(AtivoRequest.class).gimme(AtivoRequestTemplateLoader.RANDOM));
		}
		Assert.assertEquals(5, repository.count());
		
		PaginaAtivoResponse paginadoResponse = buscarAtivosUseCase.executar(-10);
		Assert.assertEquals(2, paginadoResponse.getAtivos().size());
		Assert.assertEquals(Integer.valueOf(0), paginadoResponse.getPaginaAtual());
		Assert.assertEquals(Integer.valueOf(2), paginadoResponse.getQtdRegistrosPagina());
		Assert.assertEquals(Integer.valueOf(5), paginadoResponse.getQtdRegistrosTotais());
		
		paginadoResponse = buscarAtivosUseCase.executar(null);
		Assert.assertEquals(2, paginadoResponse.getAtivos().size());
		Assert.assertEquals(Integer.valueOf(0), paginadoResponse.getPaginaAtual());
		Assert.assertEquals(Integer.valueOf(2), paginadoResponse.getQtdRegistrosPagina());
		Assert.assertEquals(Integer.valueOf(5), paginadoResponse.getQtdRegistrosTotais());
	}
}
