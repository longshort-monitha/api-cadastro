package br.com.monitha.longshort.apicadastro.core.dataprovider.kafka;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import br.com.monitha.longshort.apicadastro.core.usecase.dto.AtivoRequest;
import br.com.monitha.longshort.apicadastro.core.usecase.gateway.GerarEventoCadastroAtivoGateway;

@Component
@Profile("prod")
public class ProducerKafka implements GerarEventoCadastroAtivoGateway{

	@Value(value = "${app.kafka.cadastroativos.topic-name}")
	private String kafkaTopicName;

	private final KafkaTemplate<String, AtivoRequest> kafkaTemplate;

	public ProducerKafka(KafkaTemplate<String, AtivoRequest> kafkaTemplate) {
		this.kafkaTemplate = kafkaTemplate;
	}

	
	@Override
	public void gerarEvento(AtivoRequest ativo) {
		kafkaTemplate.send(kafkaTopicName, ativo);
	}

}
