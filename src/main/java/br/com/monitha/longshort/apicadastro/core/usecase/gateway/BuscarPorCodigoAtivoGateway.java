package br.com.monitha.longshort.apicadastro.core.usecase.gateway;

import java.util.Optional;

import br.com.monitha.longshort.apicadastro.core.entity.Ativo;

public interface BuscarPorCodigoAtivoGateway{
	
	Optional<Ativo> buscarPorCodigoAtivo(String codigo);

}
