package br.com.monitha.longshort.apicadastro.core.usecase.gateway;

public interface ConfiguracaoGateway {

	/**
	 * Retorna a quantidade de registros permitidas por pagina
	 * @return {@link Integer}
	 * */
	Integer getQuantidadeRegistrosPorPagina();
}
