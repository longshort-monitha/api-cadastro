package br.com.monitha.longshort.apicadastro.core.usecase.gateway;

import br.com.monitha.longshort.apicadastro.core.usecase.dto.AtivoRequest;

public interface GerarEventoCadastroAtivoGateway {
	
	void gerarEvento(AtivoRequest ativo);

}
