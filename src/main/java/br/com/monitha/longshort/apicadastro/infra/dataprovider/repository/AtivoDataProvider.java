package br.com.monitha.longshort.apicadastro.infra.dataprovider.repository;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

import br.com.monitha.longshort.apicadastro.core.entity.Ativo;
import br.com.monitha.longshort.apicadastro.core.usecase.dto.PaginaAtivoResponse;
import br.com.monitha.longshort.apicadastro.core.usecase.dto.PaginaAtivoResponseData;
import br.com.monitha.longshort.apicadastro.core.usecase.gateway.BuscarAtivosGateway;
import br.com.monitha.longshort.apicadastro.core.usecase.gateway.BuscarPorCodigoAtivoGateway;
import br.com.monitha.longshort.apicadastro.infra.configuration.mapper.ObjectMapperUtil;
import br.com.monitha.longshort.apicadastro.infra.dataprovider.entity.JpaAtivoEntity;
import br.com.monitha.longshort.base.gateway.BuscarPorIdGateway;
import br.com.monitha.longshort.base.gateway.DeletarGateway;
import br.com.monitha.longshort.base.gateway.SalvarGateway;

//import static br.com.monitha.longshort.apicadastro.configuration.mapper.ObjectMapperUtil.convertTo;

@Repository
public class AtivoDataProvider implements BuscarPorCodigoAtivoGateway, SalvarGateway<Ativo>, 
				BuscarPorIdGateway<Ativo, String>, DeletarGateway<String>, BuscarAtivosGateway{

	
	private final JpaAtivoRepository repository;
	
	public AtivoDataProvider(JpaAtivoRepository repository) {
		this.repository = repository;
	}

	@Override
	public Optional<Ativo> buscarPorId(String id) {
		JpaAtivoEntity jpaAtivoEntity = repository.findById(id).orElse(null);
		if (Objects.nonNull(jpaAtivoEntity)) {
			return Optional.of(ObjectMapperUtil.convertTo(jpaAtivoEntity, Ativo.class));
		} else {
			return Optional.empty();
		}
	}

	@Override
	public Ativo salvar(Ativo t) {
		JpaAtivoEntity entity = repository.save(ObjectMapperUtil.convertTo(t, JpaAtivoEntity.class));
		return ObjectMapperUtil.convertTo(entity, Ativo.class);
	}

	@Override
	public Optional<Ativo> buscarPorCodigoAtivo(String codigo) {
		List<JpaAtivoEntity> jpaAtivos = repository.findByCodigo(codigo);
		if (jpaAtivos.isEmpty()) {
			return Optional.empty();
		} else {
			return Optional.of(ObjectMapperUtil.convertTo(jpaAtivos.get(0), Ativo.class));
		}
	}

	@Override
	public Boolean deletar(String codigo) {
		List<JpaAtivoEntity> jpaAtivos = repository.findByCodigo(codigo);
		if(!jpaAtivos.isEmpty()) {
			repository.delete(jpaAtivos.get(0));
			return true;
		}
		return false;
	}

	@Override
	public PaginaAtivoResponse buscarAtivos(Integer pagina, Integer qtdRegistrosPagina) {
		Page<JpaAtivoEntity> pageJpaAtivo = repository.findAll(PageRequest.of(pagina, qtdRegistrosPagina));
		
		PaginaAtivoResponse response = new PaginaAtivoResponse();
		response.setPaginaAtual(pagina);
		response.setQtdRegistrosPagina(pageJpaAtivo.getNumberOfElements());
		response.setQtdRegistrosTotais(Math.toIntExact(pageJpaAtivo.getTotalElements()));
		pageJpaAtivo.getContent().forEach(c -> response
				.adicionarAtivo(new PaginaAtivoResponseData(c.getId(), c.getCodigo(), c.getDescricao())));
		
		return response;
	}

}
