package br.com.monitha.longshort.apicadastro.infra.configuration.mapper;

import org.springframework.context.annotation.Configuration;

import br.com.monitha.longshort.apicadastro.core.usecase.gateway.ConfiguracaoGateway;

@Configuration
public class Configuracao implements ConfiguracaoGateway{
	
	@Override
	public Integer getQuantidadeRegistrosPorPagina() {
		return 5;
	}

}
