package br.com.monitha.longshort.apicadastro.core.usecase;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.monitha.longshort.apicadastro.core.usecase.dto.AtivoRequest;
import br.com.monitha.longshort.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.monitha.longshort.base.dto.response.ListaErroEnum;
import br.com.monitha.longshort.base.dto.response.ResponseDataErro;

public abstract class BaseAtivoUseCase {
	
	protected Logger log = LoggerFactory.getLogger(this.getClass());

	protected void validarCamposObrigatorios(AtivoRequest input, AtivoResponse response) {
		
		if(StringUtils.isBlank(input.getCodigo())) {
			String msg = "O campo código é obrigatório";
			log.error(msg);
			response.getResponse().adicionarErro(
					new ResponseDataErro(msg, ListaErroEnum.CAMPOS_OBRIGATORIOS));
		}
		
		if(StringUtils.isBlank(input.getDescricao())) {
			String msg = "O campo descrição é obrigatório";
			log.error(msg);
			response.getResponse().adicionarErro(
					new ResponseDataErro(msg, ListaErroEnum.CAMPOS_OBRIGATORIOS));
		}
	}
}
