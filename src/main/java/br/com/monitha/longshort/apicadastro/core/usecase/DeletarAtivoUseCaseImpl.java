package br.com.monitha.longshort.apicadastro.core.usecase;

import java.util.Optional;

import org.springframework.stereotype.Service;

import br.com.monitha.longshort.apicadastro.core.entity.Ativo;
import br.com.monitha.longshort.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.monitha.longshort.apicadastro.core.usecase.gateway.BuscarPorCodigoAtivoGateway;
import br.com.monitha.longshort.base.dto.response.ListaErroEnum;
import br.com.monitha.longshort.base.dto.response.ResponseDataErro;
import br.com.monitha.longshort.base.gateway.DeletarGateway;

/**
 * 
 * @author monic
 * Deleta o ativo
 */
@Service
public class DeletarAtivoUseCaseImpl implements DeletarAtivoUseCase{
	
	private final BuscarPorCodigoAtivoGateway buscarPorCodigoAtivoGateway;
	
	private final DeletarGateway<String> deletarGateway;

	public DeletarAtivoUseCaseImpl(BuscarPorCodigoAtivoGateway buscarPorCodigoAtivoGateway,
			DeletarGateway<String> deletarGateway) {
		this.buscarPorCodigoAtivoGateway = buscarPorCodigoAtivoGateway;
		this.deletarGateway = deletarGateway;
	}

	/**
	 * executa delete do ativo
	 * @param input
	 * @return {@link AtivoResponse}
	 */
	@Override
	public AtivoResponse executar(String codigo) {
		AtivoResponse response = new AtivoResponse();
		response.setCodigo(codigo);
		
		//TODO: chamar use case de busca de ativo por codigo e não usar gateway
		Optional<Ativo> opAtivo = buscarPorCodigoAtivoGateway.buscarPorCodigoAtivo(codigo);
		if(opAtivo.isPresent()) {
			boolean deletado = deletarGateway.deletar(codigo);
			if(deletado) {
				response.setId(opAtivo.get().getId());
				response.setDescricao(opAtivo.get().getDescricao());
			} else {
			response.getResponse().adicionarErro(new ResponseDataErro(
					"Ativo de código " + codigo + " não foi deletado. Favor contatar a equipe de suporte", 
					ListaErroEnum.NAO_FOI_POSSIVEL_DELETAR));
			}
		} else {
			response.getResponse().adicionarErro(new ResponseDataErro(
					"Ativo de código " + codigo + " não existe", ListaErroEnum.REGISTRO_NAO_ENCONTRADO));
		}
		
		return response;
	}
	
	

}
