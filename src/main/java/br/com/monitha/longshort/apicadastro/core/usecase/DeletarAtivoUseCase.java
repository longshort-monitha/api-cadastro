package br.com.monitha.longshort.apicadastro.core.usecase;

import br.com.monitha.longshort.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.monitha.longshort.base.usecase.BaseUseCase;

/**
 * 
 * @author monic
 * Deleta o ativo
 */
public interface DeletarAtivoUseCase extends BaseUseCase<String, AtivoResponse>{

}
