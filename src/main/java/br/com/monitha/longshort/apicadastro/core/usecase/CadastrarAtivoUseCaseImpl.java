package br.com.monitha.longshort.apicadastro.core.usecase;

import org.springframework.stereotype.Service;

import br.com.monitha.longshort.apicadastro.core.entity.Ativo;
import br.com.monitha.longshort.apicadastro.core.usecase.dto.AtivoRequest;
import br.com.monitha.longshort.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.monitha.longshort.apicadastro.core.usecase.gateway.BuscarPorCodigoAtivoGateway;
import br.com.monitha.longshort.apicadastro.core.usecase.gateway.GerarEventoCadastroAtivoGateway;
import br.com.monitha.longshort.base.dto.response.ListaErroEnum;
import br.com.monitha.longshort.base.dto.response.ResponseDataErro;
import br.com.monitha.longshort.base.gateway.SalvarGateway;

/**
 * 
 * @author monic
 * Cadastra o ativo
 */
@Service
public class CadastrarAtivoUseCaseImpl extends BaseAtivoUseCase implements CadastrarAtivoUseCase{
	
	private final SalvarGateway<Ativo> salvarGateway;
	private final BuscarPorCodigoAtivoGateway buscarPorCodigoAtivoGateway;
	private final GerarEventoCadastroAtivoGateway gerarEventoGateway;

	public CadastrarAtivoUseCaseImpl(SalvarGateway<Ativo> salvarGateway,
			BuscarPorCodigoAtivoGateway buscarPorCodigoAtivoGateway,
			GerarEventoCadastroAtivoGateway gerarEventoGateway) {
		this.salvarGateway = salvarGateway;
		this.buscarPorCodigoAtivoGateway = buscarPorCodigoAtivoGateway;
		this.gerarEventoGateway = gerarEventoGateway;
	}

	/**
	 * executa cadastro do ativo
	 * @param input
	 * @return {@link AtivoResponse}
	 */
	@Override
	public AtivoResponse executar(AtivoRequest input) {
		AtivoResponse response = new AtivoResponse();
		response.setCodigo(input.getCodigo());
		response.setDescricao(input.getDescricao());
		
		validarCamposObrigatorios(input, response);
		if(response.getResponse().getErros().size() > 0) {
			return response;
		}
		
		if(buscarPorCodigoAtivoGateway.buscarPorCodigoAtivo(input.getCodigo()).isPresent()){
			response.getResponse().adicionarErro(
					new ResponseDataErro("Ativo já cadastrado com código " + input.getCodigo(), ListaErroEnum.DUPLICIDADE));
		} else {
		
		Ativo ativo = new Ativo();
		ativo.setCodigo(input.getCodigo());
		ativo.setDescricao(input.getDescricao());
		salvarGateway.salvar(ativo);	
		gerarEventoGateway.gerarEvento(input);
		response.setId(ativo.getId());
		
		}
		
		return response;
	}	

}
