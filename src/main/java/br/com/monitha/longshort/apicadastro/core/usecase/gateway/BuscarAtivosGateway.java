package br.com.monitha.longshort.apicadastro.core.usecase.gateway;

import br.com.monitha.longshort.apicadastro.core.usecase.dto.PaginaAtivoResponse;

public interface BuscarAtivosGateway{
	
	PaginaAtivoResponse buscarAtivos(Integer pagina, Integer qtdRegistrosPagina);

}
