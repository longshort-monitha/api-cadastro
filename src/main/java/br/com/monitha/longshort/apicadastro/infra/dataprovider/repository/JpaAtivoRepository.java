package br.com.monitha.longshort.apicadastro.infra.dataprovider.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.monitha.longshort.apicadastro.infra.dataprovider.entity.JpaAtivoEntity;

import java.lang.String;
import java.util.List;

public interface JpaAtivoRepository extends JpaRepository<JpaAtivoEntity, String>{
	
	List<JpaAtivoEntity> findByCodigo(String codigo);

}

