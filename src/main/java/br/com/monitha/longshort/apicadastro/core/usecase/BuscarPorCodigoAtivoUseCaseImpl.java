package br.com.monitha.longshort.apicadastro.core.usecase;

import java.util.Optional;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import br.com.monitha.longshort.apicadastro.core.entity.Ativo;
import br.com.monitha.longshort.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.monitha.longshort.apicadastro.core.usecase.gateway.BuscarPorCodigoAtivoGateway;
import br.com.monitha.longshort.base.dto.response.ListaErroEnum;
import br.com.monitha.longshort.base.dto.response.ResponseDataErro;

/**
 * 
 * @author monic
 * Cadastra o ativo
 */
@Service
public class BuscarPorCodigoAtivoUseCaseImpl implements BuscarPorCodigoAtivoUseCase{
	
	private final BuscarPorCodigoAtivoGateway buscarPorCodigoAtivoGateway;
	
	private Logger log = LoggerFactory.getLogger(BuscarPorCodigoAtivoUseCaseImpl.class);

	public BuscarPorCodigoAtivoUseCaseImpl(BuscarPorCodigoAtivoGateway buscarPorCodigoAtivoGateway) {
		this.buscarPorCodigoAtivoGateway = buscarPorCodigoAtivoGateway;
	}

	/**
	 * Busca o código do ativo
	 * @param código
	 * @return {@link AtivoResponse}
	 */
	@Override
	public AtivoResponse executar(String codigo) {
		log.info("Buscando o ativo pelo código "+codigo);
		AtivoResponse response = new AtivoResponse();
		response.setCodigo(codigo);
		
		
		validarCamposObrigatorios(codigo, response);
		if(!response.getResponse().getErros().isEmpty()) {
			return response;
		}
		
		Optional<Ativo> opAtivo = buscarPorCodigoAtivoGateway.buscarPorCodigoAtivo(codigo);
		
		if (opAtivo.isPresent()) {
			log.debug("Ativo encontrado com o código "+codigo);
			response.setId(opAtivo.get().getId());
			response.setDescricao(opAtivo.get().getDescricao());
		} else {
			log.debug("Ativo não encontrado com o código "+codigo);
			response.getResponse().adicionarErro(new ResponseDataErro("Ativo não encontrado com o código "+codigo,
					ListaErroEnum.REGISTRO_NAO_ENCONTRADO));
		}
			
		return response;
	}

	private void validarCamposObrigatorios(String codigo, AtivoResponse response) {
		log.debug("Validando campos obrigatórios");
		log.debug("Código informado "+codigo);
		if(StringUtils.isBlank(codigo)) {
			response.getResponse().adicionarErro(new ResponseDataErro("Campo código é obrigatório",
					ListaErroEnum.CAMPOS_OBRIGATORIOS));
		}
		
	}	

}
