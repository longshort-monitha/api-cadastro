package br.com.monitha.longshort.apicadastro.core.usecase.dto;

import java.util.ArrayList;
import java.util.List;

import br.com.monitha.longshort.base.dto.response.BaseResponse;

/**
 * 
 * @author monic
 * Resposta para a paginação do controller buscarTodos
 */
public class PaginaAtivoResponse extends BaseResponse {
	
	private Integer paginaAtual;
	private Integer qtdRegistrosTotais;
	private Integer qtdRegistrosPagina;
	private List<PaginaAtivoResponseData> ativos = new ArrayList<>();
	
	public PaginaAtivoResponse() {
		
	}
	
	public PaginaAtivoResponse(Integer paginaAtual, Integer qtdRegistrosTotais, Integer qtdRegistrosPagina,
			List<PaginaAtivoResponseData> ativos) {
		this.paginaAtual = paginaAtual;
		this.qtdRegistrosTotais = qtdRegistrosTotais;
		this.qtdRegistrosPagina = qtdRegistrosPagina;
		this.ativos = ativos;
	}
	
	public Integer getPaginaAtual() {
		return paginaAtual;
	}
	public void setPaginaAtual(Integer paginaAtual) {
		this.paginaAtual = paginaAtual;
	}
	public Integer getQtdRegistrosTotais() {
		return qtdRegistrosTotais;
	}
	public void setQtdRegistrosTotais(Integer qtdRegistrosTotais) {
		this.qtdRegistrosTotais = qtdRegistrosTotais;
	}
	public Integer getQtdRegistrosPagina() {
		return qtdRegistrosPagina;
	}
	public void setQtdRegistrosPagina(Integer qtdRegistrosPagina) {
		this.qtdRegistrosPagina = qtdRegistrosPagina;
	}
	public List<PaginaAtivoResponseData> getAtivos() {
		return ativos;
	}
	public void adicionarAtivo(PaginaAtivoResponseData ativoPaginado) {
		this.ativos.add(ativoPaginado);
	}
	
		

}
