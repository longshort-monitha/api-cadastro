package br.com.monitha.longshort.apicadastro.core.usecase;

import br.com.monitha.longshort.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.monitha.longshort.base.usecase.BaseUseCase;

/**
 * Buscar pro código ativo
 * 
 * @author monic
 * 
 */
public interface BuscarPorCodigoAtivoUseCase extends BaseUseCase<String, AtivoResponse>{

}
