package br.com.monitha.longshort.apicadastro.core.usecase;

import br.com.monitha.longshort.apicadastro.core.usecase.dto.PaginaAtivoResponse;
import br.com.monitha.longshort.base.usecase.BaseUseCase;

/**
 * 
 * @author monic
 * Buscar o ativo
 */
public interface BuscarAtivosUseCase extends BaseUseCase<Integer, PaginaAtivoResponse>{

}
