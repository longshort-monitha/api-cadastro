package br.com.monitha.longshort.apicadastro.infra.entrypoint.http.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.monitha.longshort.apicadastro.core.usecase.AtualizarAtivoUseCase;
import br.com.monitha.longshort.apicadastro.core.usecase.BuscarAtivosUseCase;
import br.com.monitha.longshort.apicadastro.core.usecase.BuscarPorCodigoAtivoUseCase;
import br.com.monitha.longshort.apicadastro.core.usecase.CadastrarAtivoUseCase;
import br.com.monitha.longshort.apicadastro.core.usecase.DeletarAtivoUseCase;
import br.com.monitha.longshort.apicadastro.core.usecase.dto.AtivoRequest;
import br.com.monitha.longshort.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.monitha.longshort.apicadastro.core.usecase.dto.PaginaAtivoResponse;
import br.com.monitha.longshort.base.dto.response.BaseResponse;
import br.com.monitha.longshort.base.dto.response.ListaErroEnum;
import br.com.monitha.longshort.base.dto.response.ResponseDataErro;

/**
 * Controller para acesso aos recursos do ativo
 * Exemplo de acesso
 * POST http://localhost.8080/api/v1/ativos (endpoint)
 * 
 * @author monic
 *
 */
@RestController
@RequestMapping("/api/v1/ativos")
public class AtivoController {
	
	private final CadastrarAtivoUseCase cadastarUseCase;
	
	private final DeletarAtivoUseCase deletarAtivoUseCase;
	
	private final BuscarPorCodigoAtivoUseCase buscarPorCodigoUseCase;
	
	private final AtualizarAtivoUseCase atualizarAtivoUseCase;
	
	private final BuscarAtivosUseCase buscarAtivosUseCase;
	
	private Logger log = LoggerFactory.getLogger(AtivoController.class);
	
	public AtivoController(CadastrarAtivoUseCase usecase, DeletarAtivoUseCase deletarAtivoUseCase,
			BuscarPorCodigoAtivoUseCase buscarPorCodigoUseCase, AtualizarAtivoUseCase atualizarAtivoUseCase,
			BuscarAtivosUseCase buscarAtivosUseCase) {
		this.cadastarUseCase = usecase;
		this.deletarAtivoUseCase = deletarAtivoUseCase;
		this.buscarPorCodigoUseCase = buscarPorCodigoUseCase;
		this.atualizarAtivoUseCase = atualizarAtivoUseCase;
		this.buscarAtivosUseCase = buscarAtivosUseCase;
	}

	/**
	 * @author monic
	 * Cadastra o ativo. Exemplo de chamada válida: 
	 * POST http://localhost:8080/api/v1/ativos
	 * {
	 * 	"codigo":"PETR4",
	 *  "descricao":"PETROBRAS"
	 * }
	 */
	@PostMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<AtivoResponse> cadastrarAtivo(@RequestBody AtivoRequest request) {
		log.info("Cadastrando o ativo " +request.getCodigo(), request);
		AtivoResponse cadastrarResponse = cadastarUseCase.executar(request);
		log.info("Ativo " +cadastrarResponse.getId()+ " cadastrado com sucesso", cadastrarResponse);
		return contruirResponse(cadastrarResponse);
	}
	
	/**
	 * @author monic
	 * Deleta o ativo. Exemplo de chamada válida: 
	 * DELETE http://localhost:8080/api/v1/ativos/{codigo} 
	 */
	@DeleteMapping(value = "/{codigo}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<AtivoResponse> deletarAtivo(@PathVariable("codigo") String codigo) {
		log.info("Deletando ativo "+codigo);
		AtivoResponse deletarResponse = deletarAtivoUseCase.executar(codigo);
		return contruirResponse(deletarResponse);
		
	}
	
	/**
	 * @author monic
	 * Busca o ativo através do código. Exemplo de chamada válida: 
	 * GET http://localhost:8080/api/v1/ativos/{codigo} 
	 */
	@GetMapping(value = "/{codigo}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<AtivoResponse> buscarPorCodigoAtivo(@PathVariable("codigo") String codigo) {
		log.info("Buscando o ativo "+codigo);
		AtivoResponse buscarPorCodigoResponse = buscarPorCodigoUseCase.executar(codigo);
		log.info("Resposta para a busca do ativo ", codigo);
		return contruirResponse(buscarPorCodigoResponse);	
	}


	/**
	 * @author monic
	 * Atualiza o ativo através do código. Exemplo de chamada válida: 
	 * PUT http://localhost:8080/api/v1/ativos/{codigo} 
	 */
	@PutMapping(value = "/{codigo}", consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<AtivoResponse> atualizarAtivo(@RequestBody String descricao, @PathVariable("codigo") String codigo) {
		AtivoRequest request = new AtivoRequest();
		request.setCodigo(codigo);
		request.setDescricao(descricao);
		AtivoResponse atualizarAtivoResponse = atualizarAtivoUseCase.executar(request);
		return contruirResponse(atualizarAtivoResponse);	
	}
	
	/**
	 * @author monic
	 * Busca os ativos. Exemplo de chamada válida: 
	 * GET http://localhost:8080/api/v1/ativos?pagina=10 
	 */
	@GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<PaginaAtivoResponse> buscarAtivos(@RequestParam("pagina") Integer pagina) {
		PaginaAtivoResponse paginaResponse = buscarAtivosUseCase.executar(pagina);
		return contruirResponse(paginaResponse);	
	}

	private <T extends BaseResponse> ResponseEntity<T> contruirResponse(T response) {
		log.debug("Iniciando a construção do HTTP");
		if (response.getResponse().getErros().isEmpty()) {
			log.info("Resposta sem erros (200 - OK)", response); 
			return ResponseEntity.ok(response);
		}else {
			log.error("Ocorreram erros ao processar sua solicitação", response);
			List<ResponseDataErro> erros = response.getResponse().getErros();
			ResponseDataErro erro = erros.get(0);
			
			if (erro.getTipo().equals(ListaErroEnum.CAMPOS_OBRIGATORIOS)) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
			} else if (erro.getTipo().equals(ListaErroEnum.DUPLICIDADE)) {
				return ResponseEntity.status(HttpStatus.CONFLICT).body(response);
			}else if (erro.getTipo().equals(ListaErroEnum.REGISTRO_NAO_ENCONTRADO)) {
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
			}
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
		}
	}

}
