package br.com.monitha.longshort.apicadastro.core.usecase;

import br.com.monitha.longshort.apicadastro.core.usecase.dto.AtivoRequest;
import br.com.monitha.longshort.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.monitha.longshort.base.usecase.BaseUseCase;

/**
 * 
 * @author monic
 * Atualiza o ativo
 */
public interface AtualizarAtivoUseCase extends BaseUseCase<AtivoRequest, AtivoResponse>{

}
