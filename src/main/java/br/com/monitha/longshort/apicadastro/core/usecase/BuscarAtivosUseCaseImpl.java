package br.com.monitha.longshort.apicadastro.core.usecase;

import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import br.com.monitha.longshort.apicadastro.core.usecase.dto.PaginaAtivoResponse;
import br.com.monitha.longshort.apicadastro.core.usecase.gateway.BuscarAtivosGateway;
import br.com.monitha.longshort.apicadastro.core.usecase.gateway.ConfiguracaoGateway;

/**
 * 
 * @author monic
 * Busca os ativos
 */
@Service
public class BuscarAtivosUseCaseImpl implements BuscarAtivosUseCase{
	
	private final BuscarAtivosGateway buscarAtivosGateway;
	
	private Logger log = LoggerFactory.getLogger(BuscarAtivosUseCaseImpl.class);
	
	private final ConfiguracaoGateway configuracaoGateway;
	
	public BuscarAtivosUseCaseImpl(BuscarAtivosGateway buscarAtivoGateway,
			ConfiguracaoGateway configuracaoGateway) {
		this.buscarAtivosGateway = buscarAtivoGateway;
		this.configuracaoGateway = configuracaoGateway;
	}

	/**
	 * executa busca do ativo
	 * @param pagina
	 * @return {@link PaginaAtivoResponse}
	 */
	@Override
	public PaginaAtivoResponse executar(Integer pagina) {
		
		log.info("Buscando ativos da página " +pagina + ". Qtd registros máximo =  " + configuracaoGateway.getQuantidadeRegistrosPorPagina());
		
		if (Objects.isNull(pagina) || pagina < 0) {
			log.warn("Estamos setando a pagina = 0, pois a página passada é igual a = " +pagina);
			pagina = 0;
		}
		
		PaginaAtivoResponse response = buscarAtivosGateway.buscarAtivos(pagina, configuracaoGateway.getQuantidadeRegistrosPorPagina());
		log.info("Foram encontrados [" +response.getQtdRegistrosPagina() + "] registros na página " +pagina);
		log.info("Foram encontrados [" +response.getQtdRegistrosTotais() + "] registros totais");
		
		return response;
	}
	
	

}
