package br.com.monitha.longshort.apicadastro.core.dataprovider.kafka;

import org.springframework.stereotype.Component;

import br.com.monitha.longshort.apicadastro.core.usecase.dto.AtivoRequest;
import br.com.monitha.longshort.apicadastro.core.usecase.gateway.GerarEventoCadastroAtivoGateway;

@Component
public class ProducerKafkaFake implements GerarEventoCadastroAtivoGateway {
	
	@Override
	public void gerarEvento(AtivoRequest ativo) {
		// TODO Auto-generated method stub
	}

}
