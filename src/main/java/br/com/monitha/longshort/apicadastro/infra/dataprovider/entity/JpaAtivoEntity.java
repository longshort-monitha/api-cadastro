package br.com.monitha.longshort.apicadastro.infra.dataprovider.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Classe para mapeamento da tabela ativo
 * @author monic
 *
 */
@Entity
@Table(name = "Ativo")
public class JpaAtivoEntity extends JpaBaseEntity {
	
	@Column(name = "codigo", length = 10, nullable = false)
	private String codigo;
	
	@Column(name = "descricao", length = 255, nullable = false)
	private String descricao;

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	

}
