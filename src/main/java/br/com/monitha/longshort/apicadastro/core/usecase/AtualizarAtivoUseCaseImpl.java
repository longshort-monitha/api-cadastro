package br.com.monitha.longshort.apicadastro.core.usecase;

import java.util.Optional;

import org.springframework.stereotype.Service;

import br.com.monitha.longshort.apicadastro.core.entity.Ativo;
import br.com.monitha.longshort.apicadastro.core.usecase.dto.AtivoRequest;
import br.com.monitha.longshort.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.monitha.longshort.apicadastro.core.usecase.gateway.BuscarPorCodigoAtivoGateway;
import br.com.monitha.longshort.base.dto.response.ListaErroEnum;
import br.com.monitha.longshort.base.dto.response.ResponseDataErro;
import br.com.monitha.longshort.base.gateway.SalvarGateway;

/**
 * 
 * @author monic
 * Atualiza o ativo
 */
@Service
public class AtualizarAtivoUseCaseImpl extends BaseAtivoUseCase implements AtualizarAtivoUseCase{
	
	private final SalvarGateway<Ativo> salvarGateway;
	private final BuscarPorCodigoAtivoGateway buscarPorCodigoAtivoGateway;

	public AtualizarAtivoUseCaseImpl(SalvarGateway<Ativo> salvarGateway,
			BuscarPorCodigoAtivoGateway buscarPorCodigoAtivoGateway) {
		this.salvarGateway = salvarGateway;
		this.buscarPorCodigoAtivoGateway = buscarPorCodigoAtivoGateway;
	}

	/**
	 * executa atualização do ativo
	 * @param input
	 * @return {@link AtivoResponse}
	 */
	@Override
	public AtivoResponse executar(AtivoRequest input) {
		
		log.info("Iniciando a atualização do ativo de código " +input.getCodigo() + " e descrição "
								+input.getDescricao());
		
		AtivoResponse response = new AtivoResponse();
		response.setCodigo(input.getCodigo());
		response.setDescricao(input.getDescricao());
		
		validarCamposObrigatorios(input, response);
		if(response.getResponse().getErros().size() > 0) {
			return response;
		}
		
		Optional<Ativo> opAtivo = buscarPorCodigoAtivoGateway.buscarPorCodigoAtivo(input.getCodigo());
		
		if(opAtivo.isPresent()){
			Ativo ativo = opAtivo.get();
			ativo.setDescricao(input.getDescricao());
			salvarGateway.salvar(ativo);
			response.setId(ativo.getId());
		} else {
			String msg = "Ativo não encontrado com o código " + input.getCodigo();
			log.error(msg);
			response.getResponse().adicionarErro(new ResponseDataErro(msg, ListaErroEnum.REGISTRO_NAO_ENCONTRADO));
		}
		
		return response;
	}
	
	

}
